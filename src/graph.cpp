#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include <new>
using namespace std;

#include <GL/glew.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifndef NO_CUDA
#pragma message "Using CUDA (GPU)"
// pliki nagłówkowe wysokopoziomowego CUDA Runtime API
// i plik nagłówkowy interoperacyjności CUDA z OpenGL
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#else  /* NO_CUDA */
#pragma message "Using CPU only"
#endif /* NO_CUDA */

#include "../common/utils.h"
#include "../common/shader_utils.h"

#if !defined(USE_GLUT) && !defined(USE_GLFW)
#define USE_GLUT
#endif /* domyślna biblioteka okienkowa */
#if   defined(USE_GLUT)
#include "window_glut.h"
#elif defined(USE_GLFW)
#include "window_glfw.h"
#endif /* dołączenie odpowiedniego pliku nagłówkowego */

#include "arcball.hpp"
#include "arcballhelper.hpp"

#include "graph_func.h"
#ifndef NO_CUDA
#include "graph_func_GPU.h"
#endif

GLuint program, program_simple;
GLint attribute_coord2d;
GLint attribute_value;
GLint uniform_vertex_transform;
GLint uniform_color;
GLint program_simple_vertex;
GLint program_simple_vertex_transform;
GLint program_simple_color;

float xmin = -1.0, xmax =  1.0; // zakres wartości x-ów
float ymin = -1.0, ymax =  1.0; // zakres wartości y-ów

float offset_x = 0.0; // przesunięcie funkcji w prawo
float offset_y = 0.0; // przesunięcie funkcji w górę
float scale = 1.0; // scale>1 to powiększenie szczegółów (zoom)
// powiększenie nie skaluje przesunięcia, tzn. przesunięcie o połowę (o 0.5)
// po dwukrotnym powiekszeniu jest przesunięciem o całość (dalej o 0.5)

glm::mat4 model = glm::mat4(1.0f);
float step_angle = glm::radians(5.0f); // kąt kroku obracania w radianach

float anim_rotation_angle = 0.0f;
int   anim_start_time_ms = 0;

#define WINDOW_WIDTH  640
#define WINDOW_HEIGHT 640

Arcball arcball(WINDOW_WIDTH, WINDOW_HEIGHT, 1.5f, true, true);
ArcballHelper arcballHelper;

bool rotate = false;
bool perspective = true;
bool polygonoffset = true;
bool show_cube = true;
GLfloat cube_line_width = 1.0;
GLfloat line_width_range[2];

enum vbo_indices {
	vboVertices = 0,
	vboValues,
	vboIndicesLines,
	vboIndicesSurface,
	N_vbo,
};
GLuint vbo[N_vbo];

#define MESH_NI  41
#define MESH_NJ  41

#define MESH_NY MESH_NI
#define MESH_NX MESH_NJ

glm::vec2 *vertices = NULL; // współrzędne punktów siatki
GLfloat *values = NULL; // wartości funkcji w jej współrzędnych
GLfloat zscale = 1.0; // skalowanie wartości funkcji (by zakres: -1..1)
size_t sizeof_vertices, sizeof_values;

/*
 * Definicja struktury grupującej uchwyt obiektu bufora w OpenGL
 * i dane z nim związane, z zasobem w przestrzeni adresowej CUDA
 */
typedef struct mappedBuffer {
	GLuint vbo;      // uchwyt obiektu bufora OpenGL
	//GLuint typeSize; // rozmiar jednego elementu bufora
#ifndef NO_CUDA
	struct cudaGraphicsResource *cudaResource;
#endif /* !NO_CUDA */
} mappedBuffer_t;

#ifndef NO_CUDA
bool use_GPU = false;
mappedBuffer_t values_resource;
#endif /* !NO_CUDA */

#ifndef NDEBUG
#define check_gl_errors() _check_gl_errors(__FILE__,__LINE__)
#else
#define check_gl_errors() ((void)0)
#endif

void _check_gl_errors(const char *file, unsigned int line)
{
	GLenum err;
	while ((err = glGetError()) != GL_NO_ERROR) {
		switch (err) {
		case GL_INVALID_ENUM:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_ENUM");
			break;
		case GL_INVALID_VALUE:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_VALUE");
			break;
		case GL_INVALID_OPERATION:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_OPERATION");
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_FRAMEBUFFER_OPERATION");
			break;
		case GL_OUT_OF_MEMORY:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_OUT_OF_MEMORY");
			break;
		case GL_STACK_UNDERFLOW:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_STACK_UNDERFLOW");
			break;
		case GL_STACK_OVERFLOW:
			fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_STACK_OVERFLOW");
			break;
		default:
			fprintf(stderr, "%s:%d glError - [%d]\n", file, line, err);
			break;
		}
	}
}

void init_mesh_vertices(glm::vec2 *vertices, int NI, int NJ)
{
	for (int i = 0; i < NI; i++) {
		for (int j = 0; j < NJ; j++) {
			vertices[i*NJ + j].x = grid_pos(j, NJ);
			vertices[i*NJ + j].y = grid_pos(i, NI);
		}
	}
}


void values_to_gpu(GLfloat *values, GLsizeiptr size, GLuint val_vbo)
{
	glBindBuffer(GL_ARRAY_BUFFER, val_vbo);
	glBufferData(GL_ARRAY_BUFFER, size, values, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void prep_values(GLfloat *values, float zscale,
                 float offset_x, float offset_y, float  xyscale,
                 GLsizeiptr size, GLuint val_vbo)
{
	// 'values' mogło by być zmienną lokalną, ale jest dosyć duże

	calc_values(values, MESH_NX, MESH_NY,
	            zscale, offset_x, offset_y, xyscale);
	values_to_gpu(values, size, val_vbo);
}

#ifndef NO_CUDA
void prep_values_GPU(struct mappedBuffer valuesVBO, float zscale,
                     float offset_x, float offset_y, float  xyscale,
                     GLsizeiptr size, GLuint val_vbo)
{
	GLfloat *values_dev;
	size_t   values_size;

	cudaGraphicsMapResources(1, &valuesVBO.cudaResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**)&values_dev, &values_size,
	                                     valuesVBO.cudaResource);

	calc_values_GPU(values_dev, MESH_NX, MESH_NY,
	                zscale, offset_x, offset_y, xyscale);

	cudaGraphicsUnmapResources(1, &valuesVBO.cudaResource, NULL);
}

void init_cuda_gl_interop(struct mappedBuffer *mbuf, GLuint val_vbo)
{
	eprintf("[%s]\n", __func__);
	mbuf->vbo = val_vbo;

	eprintf("- registering VBO [%u]...\n", (unsigned int)mbuf->vbo);
	cudaGraphicsGLRegisterBuffer(&(mbuf->cudaResource), mbuf->vbo,
	                             cudaGraphicsMapFlagsWriteDiscard);
	eprintf("- VBO created and registered [OpenGL:%u/CUDA res:%p] (done).\n",
	        mbuf->vbo, mbuf->cudaResource);
}
#endif

#ifdef LINK_RESOURCES
//#ifdef LINK_RESOURCES_LD
// patrz `objdump -x *.obj`
extern char _binary_graph_v_glsl_start; // "graph.v.glsl"
extern char _binary_graph_v_glsl_end;
extern char _binary_graph_f_glsl_start; // "graph.f.glsl"
extern char _binary_graph_f_glsl_end;

extern char _binary_simple_v_glsl_start; // "simple.v.glsl"
extern char _binary_simple_v_glsl_end;
extern char _binary_simple_f_glsl_start; // "simple.f.glsl"
extern char _binary_simple_f_glsl_end;
//#else
//// użycie https://github.com/graphitemaster/incbin
//#define INCBIN_PREFIX _binary_
//#define INCBIN_STYLE INCBIN_STYLE_SNAKE
//#include "../incbin/incbin.h"
//INCBIN(graph_v_glsl, "graph.v.glsl")
//INCBIN(graph_f_glsl, "graph.f.glsl")
//// This translation unit now has three symbols
//// const unsigned char _binary_graph_v_glsl_data[];
//// const unsigned char *_binary_graph_v_glsl_end;
//// const unsigned int _binary_graph_v_glsl_size;
//#fi

/* zapewnij zakończenie źródeł znakiem NUL,
 * zwróć informację czy się powiodło */
int ensure_ends_with_nul(char *source_end)
{
	/* sprawdza tuż za plikiem/źródłem jest znaki '\0' (NUL) */
	if ((*source_end) != '\0') {
		/* sprawdza czy plik/źródło kończy się znakiem spacji,
		 * czyli czy jest miejsce na dopisanie/nadpisaie '\0' */
		if (isspace(source_end[-1])) {
			source_end[-1] = '\0';
		} else {
			return 0;
		}
	}
	return 1;
}
//#endif
#endif

int init_resources(void)
{
	eprintf("[%s]\n", __func__);

	// alokacja pamięci
	eprintf("- allocating memory:\n");
	vertices = new (nothrow) glm::vec2 [MESH_NI*MESH_NJ];
	sizeof_vertices = MESH_NI*MESH_NJ*sizeof(vertices[0]);
	if (!vertices) {
		return 0;
	}
	eprintf("  - allocated %zd bytes (%d elems) for vertices: %p\n",
	        sizeof_vertices, MESH_NI*MESH_NJ, vertices);
	values   = new (nothrow) GLfloat   [MESH_NI*MESH_NJ];
	sizeof_values   = MESH_NI*MESH_NJ*sizeof(values[0]);
	if (!values) {
		delete [] vertices;
		return 0;
	}
	eprintf("  - allocated %zd bytes (%d elems) for values:   %p\n",
	        sizeof_values, MESH_NI*MESH_NJ, values);


#ifdef LINK_RESOURCES
	/* zapewnij zakończenie źródeł znakiem NUL. */
	if (ensure_ends_with_nul(&_binary_graph_v_glsl_end) &&
	    ensure_ends_with_nul(&_binary_graph_f_glsl_end) &&
	    ensure_ends_with_nul(&_binary_simple_v_glsl_end) &&
	    ensure_ends_with_nul(&_binary_simple_f_glsl_end)) {
		/* korzystaj z włączonych (embedded) źródeł */
		program = create_program_source(
			&_binary_graph_v_glsl_start,
			&_binary_graph_f_glsl_start,
			"graph.v.glsl", "graph.f.glsl"
		);
		program_simple = create_program_source(
			&_binary_simple_v_glsl_start,
			&_binary_simple_f_glsl_start,
			"simple.v.glsl", "simple.f.glsl"
		);
	} else {
		/* czytaj z pliku */
		program = create_program(
			 "graph.v.glsl", "graph.f.glsl"
		);
		program_simple = create_program(
			 "simple.v.glsl", "simple.f.glsl"
		);
	}
#else
	/* czytaj z pliku */
	program        = create_program("graph.v.glsl",  "graph.f.glsl");
	program_simple = create_program("simple.v.glsl", "simple.f.glsl");
#endif
	if (program == 0 || program_simple == 0)
		return 0;
	eprintf("- created programs %d and %d\n", program, program_simple);

	attribute_coord2d = get_attrib(program, "coord2d");
	attribute_value   = get_attrib(program, "value");
	uniform_vertex_transform = get_uniform(program, "vertex_transform");
	uniform_color = get_uniform(program, "color");

	program_simple_vertex = get_attrib(program_simple, "vertex");
	program_simple_vertex_transform = get_uniform(program_simple, "vertex_transform");
	program_simple_color = get_uniform(program_simple, "color");

	if (attribute_coord2d == -1 ||
	    attribute_value   == -1 ||
	    uniform_vertex_transform == -1 ||
	    uniform_color == -1)
		return 0;
	if (program_simple_vertex == -1 ||
	    program_simple_vertex_transform == -1 ||
	    program_simple_color == -1)
		return 0;
	eprintf("   - got attribute and uniform parameters\n");

	// Create vertex buffer objects
	glGenBuffers(N_vbo, vbo);
	eprintf("- generated %d buffers\n", N_vbo);

	// Create an array for MESH_NX * MESH_NY vertices
	init_mesh_vertices(vertices, MESH_NI, MESH_NJ);

	// Tell OpenGL to copy our array to the buffer objects
	glBindBuffer(GL_ARRAY_BUFFER, vbo[vboVertices]);
	glBufferData(GL_ARRAY_BUFFER, sizeof_vertices, vertices, GL_STATIC_DRAW);

	// Create our datapoints, store it as float, and
	// upload the attribute with our datapoints, to buffer objects
	eprintf("## rescale ##\n");
	scale_offset_from_ranges(xmin, xmax, ymin, ymax,
	                         &scale, &offset_x, &offset_y);
	eprintf("requested ranges:\n");
	eprintf("- xrange: [ %+.3f .. %+.3f ]\n", xmin, xmax);
	eprintf("- yrange: [ %+.3f .. %+.3f ]\n", ymin, ymax);
	eprintf("calculated parameters:\n");
	eprintf("- scale  = %g\n", scale);
	eprintf("- offset = ( % .3f, % .3f )\n", offset_x, offset_y);
	eprintf("effective ranges\n");
	float xmin_eff = xmin, xmax_eff = xmax;
	float ymin_eff = ymin, ymax_eff = ymax;
	range_from_func_xy(scale, offset_x, &xmin_eff, &xmax_eff);
	range_from_func_xy(scale, offset_y, &ymin_eff, &ymax_eff);
	eprintf("- xrange: [ %+.3f .. %+.3f ]\n", xmin_eff, xmax_eff);
	eprintf("- yrange: [ %+.3f .. %+.3f ]\n", ymin_eff, ymax_eff);
	prep_values(values, zscale,
	            offset_x, offset_y, scale,
	            sizeof_values, vbo[vboValues]);
#ifndef NO_CUDA
	use_GPU = false;
	init_cuda_gl_interop(&values_resource, vbo[vboValues]);
#endif /* !NO_CUDA */

	// Create an array of indices into the vertex array that traces both horizontal and vertical lines
	GLushort indices[(MESH_NI-1) * (MESH_NJ-1) * 6]; // max(101*100*4, 100*100*6)
	int i = 0;

	for (int y = 0; y < MESH_NY; y++) {
		for (int x = 0; x < (MESH_NX-1); x++) {
			indices[i++] = y * MESH_NX + x;
			indices[i++] = y * MESH_NX + x + 1;
		}
	}

	for (int x = 0; x < MESH_NX; x++) {
		for (int y = 0; y < (MESH_NY-1); y++) {
			indices[i++] = y       * MESH_NX + x;
			indices[i++] = (y + 1) * MESH_NX + x;
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[vboIndicesLines]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, i * sizeof(*indices), indices, GL_STATIC_DRAW);

	// Create another array of indices that describes all the triangles needed to create a completely filled surface
	i = 0;

	for (int y = 0; y < MESH_NY-1; y++) {
		for (int x = 0; x < MESH_NX-1; x++) {
			indices[i++] = y *       MESH_NX + x;
			indices[i++] = y *       MESH_NX + x + 1;
			indices[i++] = (y + 1) * MESH_NX + x + 1;

			indices[i++] = y *       MESH_NX + x;
			indices[i++] = (y + 1) * MESH_NX + x + 1;
			indices[i++] = (y + 1) * MESH_NX + x;
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[vboIndicesSurface]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	return 1;
}

/*
 * UWAGA: ponieważ funkcja ta wykonywana jest póżno w stosie wywołań,
 * to zasoby OpenGL mogły już zostać zwolnione przez deinicjalizację OpenGL
 * lub usunięcie kontekstu OpenGL ze względu na usunięciem okna.
 */
void free_gl_resources(void)
{
	eprintf("[%s]\n", __func__);

	// odwiązanie buforów
	eprintf("- unbinding OpenGL buffers...\n");
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// usunięcie obiektów bufora (vertex buffer objects)
	eprintf("- deleting %d buffer objects...\n", N_vbo);
	eprintf("  - buffer:");
	for (int vbo_idx = 0; vbo_idx < N_vbo; vbo_idx++)
		eprintf(" %d%s", vbo[vbo_idx], glIsBuffer(vbo[vbo_idx]) == GL_TRUE ? "" : "*");
	eprintf("\n");
	glDeleteBuffers(N_vbo, vbo);

	// usunięcie shaderów (dla jednego z programów)
	eprintf("- deleting program %d...\n", program);
	if (glIsProgram(program) != GL_TRUE) {
		eprintf("  !!! %d is not a program\n", program);
	}
	GLint   count = 0, shader_type;
	GLsizei nshaders = 0;
#define MAX_SHADERS 2
	GLuint  shaders[MAX_SHADERS];
	glGetProgramiv(program, GL_ATTACHED_SHADERS, &count);
	eprintf("  - found %d attached shaders\n", count);
	glGetAttachedShaders(program, MAX_SHADERS, &nshaders, shaders);
	eprintf("  - got   %d attached shaders\n", nshaders);
	nshaders = nshaders >= MAX_SHADERS ? MAX_SHADERS : nshaders;
	for (int si = 0; si < nshaders; si++) {
		glGetShaderiv(shaders[si], GL_SHADER_TYPE, &shader_type);
		if (shader_type == GL_INVALID_VALUE) {
			eprintf("  !!! [%d] %d is not a value generated by OpenGL.\n",
			        si, shaders[si]);
			continue;
		} else if (shader_type == GL_INVALID_OPERATION) {
			eprintf("  !!! [%d] %d does not refer to a shader object\n",
			        si, shaders[si]);
			continue;
		}
		eprintf("  - detaching and deleting %s shader %d (nr %d/%d)\n",
		        shader_type == GL_VERTEX_SHADER   ? "vertex  " :
		        shader_type == GL_FRAGMENT_SHADER ? "fragment" :
		        "???",
		        shaders[si], si, MAX_SHADERS);

		glDetachShader(program, shaders[si]);
		glDeleteShader(shaders[si]);
	}

	// usunięcie obiektu programu
	eprintf("  - deleting program object %d...\n", program);
	glDeleteProgram(program);
}

void free_resources(void)
{
	eprintf("[%s]\n", __func__);

#ifndef NO_CUDA
	// usunięcie zasobów graficznych CUDA <-> OpenGL
	eprintf("- unregistering graphics resource %p attached to buffer %d...\n",
	        values_resource.cudaResource, values_resource.vbo);
	cudaGraphicsUnregisterResource(values_resource.cudaResource);
#endif

	// zwolnienie pamięci
	eprintf("- freeing memory...\n");
	eprintf("  - delete [] vertices: %p\n", vertices);
	delete [] vertices;
	eprintf("  - delete [] values:   %p\n", values);
	delete [] values;

	eprintf("(done)\n");
}

void draw_cube(glm::mat4 vertex_transform, GLfloat color_rgba[4])
{
	glUseProgram(program_simple);

	glUniformMatrix4fv(program_simple_vertex_transform, 1, GL_FALSE,
	                   glm::value_ptr(vertex_transform));
	glUniform4fv(program_simple_color, 1, color_rgba);

	GLfloat cube_vertices[][3] = {
		// back face
		{-1.0, -1.0, -1.0},
		{ 1.0, -1.0, -1.0},
		{ 1.0,  1.0, -1.0},
		{-1.0,  1.0, -1.0},
		// front face
		{-1.0, -1.0,  1.0},
		{ 1.0, -1.0,  1.0},
		{ 1.0,  1.0,  1.0},
		{-1.0,  1.0,  1.0},
	};
	GLushort cube_lines_indices[][2] = {
		// back face
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 0},
		// front face
		{4, 5},
		{5, 6},
		{6, 7},
		{7, 4},
		// connect
		{0, 4},
		{1, 5},
		{2, 6},
		{3, 7},
	};

	glEnableVertexAttribArray(program_simple_vertex);
	glVertexAttribPointer(program_simple_vertex, 3, GL_FLOAT, GL_FALSE, 0,
	                      &(cube_vertices[0][0]));

	glPushAttrib(GL_LINE_BIT);
	glLineWidth(cube_line_width);

	glDrawElements(GL_LINES, 12*sizeof(GLushort), GL_UNSIGNED_SHORT,
	               &(cube_lines_indices[0][0]));

	glPopAttrib();

	glDisableVertexAttribArray(program_simple_vertex);
}

void display(void)
{
	glUseProgram(program);

	glm::mat4 rotation;
	if (rotate) {
		anim_rotation_angle +=
			glm::radians((elapsed_time_ms() - anim_start_time_ms) / 100.0f);
	}
	anim_start_time_ms = elapsed_time_ms();
	rotation = glm::rotate(glm::mat4(1.0f),
	                       anim_rotation_angle,
	                       glm::vec3(0.0f, 0.0f, 1.0f));

	glm::mat4 view;
	glm::mat4 projection;
	if (perspective) {
		view = glm::lookAt(glm::vec3(0.0, -2.0, 2.0),  // eye
		                   glm::vec3(0.0,  0.0, 0.0),  // center
		                   glm::vec3(0.0,  0.0, 1.0)); // up direction
		projection = glm::perspective(45.0f, 1.0f * WINDOW_WIDTH / WINDOW_HEIGHT, 0.1f, 10.0f);
	} else {
		view = glm::rotate(glm::mat4(1.0f), glm::radians(-45.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		float hratio = 1.0f * WINDOW_WIDTH / WINDOW_HEIGHT, vratio = 1.0f;
		projection = glm::ortho(-sqrtf(2.0f)*hratio,  sqrtf(2.0f)*hratio,
		                        -sqrtf(2.0f)*vratio,  sqrtf(2.0f)*vratio,
		                        -2.0f,  2.0f);
	}
	glm::mat4 vertex_transform = projection * view * model * rotation;


	glUniformMatrix4fv(uniform_vertex_transform, 1, GL_FALSE, glm::value_ptr(vertex_transform));

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	/* Draw the triangles, a little dark, with a slight offset in depth. */
	GLfloat grey[4] = { 0.5, 0.5, 0.5, 1 };
	glUniform4fv(uniform_color, 1, grey);

	glEnable(GL_DEPTH_TEST);

	if (polygonoffset) {
		glPolygonOffset(1, 0);
		glEnable(GL_POLYGON_OFFSET_FILL);
	}

	glEnableVertexAttribArray(attribute_coord2d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[vboVertices]);
	glVertexAttribPointer(attribute_coord2d, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(attribute_value);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[vboValues]);
	glVertexAttribPointer(attribute_value, 1, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[vboIndicesSurface]);
	glDrawElements(GL_TRIANGLES, (MESH_NX-1) * (MESH_NY-1) * 6, GL_UNSIGNED_SHORT, 0);

	glPolygonOffset(0, 0);
	glDisable(GL_POLYGON_OFFSET_FILL);

	/* Draw the grid, very bright */
	GLfloat bright[4] = { 2, 2, 2, 1 };
	glUniform4fv(uniform_color, 1, bright);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[vboIndicesLines]);
	glDrawElements(GL_LINES, 4*MESH_NX*MESH_NY - 2*(MESH_NX + MESH_NY), GL_UNSIGNED_SHORT, 0);

	/* Stop using the vertex buffer object */
	glDisableVertexAttribArray(attribute_coord2d);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	GLfloat cube_color[4] = { 1.0, 0.1, 0.1, 1.0 };
	if (show_cube)
		draw_cube(vertex_transform, cube_color);

	check_gl_errors();
	finalize_display();
}

void special(int key, int x, int y)
{
	bool needs_replot = false;

	switch (key) {
	case KEY_ESC: // niektóre biblioteki traktują ESC jako klawisz specjalny
		//done_window();
		exit_event_loop();
		break;

	case KEY_F1:
		show_cube = !show_cube;
		printf("%s model cube\n", show_cube ? "Showing" : "Not showing");
		break;
	case KEY_F2:
		perspective = !perspective;
		printf("Perspective is now %s\n", perspective ? "on" : "off");
		break;
	case KEY_F3:
		rotate = !rotate;
		printf("Rotation is now %s\n", rotate ? "on" : "off");
		break;
	case KEY_F4:
		polygonoffset = !polygonoffset;
		printf("Polygon offset is now %s\n", polygonoffset ? "on" : "off");
		break;
	case KEY_LEFT:
		offset_x -= 0.03;
		needs_replot = true;
		break;
	case KEY_RIGHT:
		offset_x += 0.03;
		needs_replot = true;
		break;
	case KEY_UP:
		offset_y += 0.03;
		needs_replot = true;
		break;
	case KEY_DOWN:
		offset_y -= 0.03;
		needs_replot = true;
		break;
	case KEY_PAGE_UP:
		scale *= 1.5;
		needs_replot = true;
		break;
	case KEY_PAGE_DOWN:
		scale /= 1.5;
		needs_replot = true;
		break;
	case KEY_HOME:
		if (offset_x != 0.0 || offset_y != 0.0 || scale != 1.0)
			needs_replot = true;

		offset_x = 0.0;
		offset_y = 0.0;
		scale = 1.0;
		break;

	case KEY_END:
		model = glm::mat4(1.0f);
		break;

#ifndef NO_CUDA
	case KEY_F5:
		use_GPU = !use_GPU;
		printf("Using calculation on %s\n", use_GPU ? "GPU with CUDA" : "CPU");
		needs_replot = true;
#endif /* !NO_CUDA */
	}

	if (needs_replot) {
		//printf("Needs replot\n");
#ifdef NO_CUDA
		prep_values(values, zscale,
		            offset_x, offset_y, scale,
		            sizeof_values, vbo[vboValues]);
#else  /* !NO_CUDA */
		if (!use_GPU) {
			prep_values(values, zscale,
			            offset_x, offset_y, scale,
			            sizeof_values, vbo[vboValues]);
		} else {
			prep_values_GPU(values_resource, zscale,
			                offset_x, offset_y, scale,
			                sizeof_values, vbo[vboValues]);
		}
#endif /* !NO_CUDA */
	}
	redisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case KEY_ESC:
		//done_window();
		exit_event_loop();
		break;

	case '.':
	case '>':
		if (cube_line_width + 1 <= line_width_range[1])
			cube_line_width += 1.0;
		break;
	case ',':
	case '<':
		if (cube_line_width - 1 >= line_width_range[0])
			cube_line_width -= 1.0;
		break;

		/* obracanie widoku */
	case 'q': case 'w': case 'e':
	case 'Q': case 'W': case 'E':
	case 'a': case 's': case 'd':
	case 'A': case 'S': case 'D':
		float rotation_angle = step_angle;
		if (isupper(key))
			rotation_angle *= 5.0f;  /* mnożnik przyspieszenia */
		switch (key) {
		case 'a': case 's': case 'd':
		case 'A': case 'S': case 'D':
			rotation_angle = -rotation_angle; /* obrót w przeciwnym kierunku */
			break;
		}
		glm::vec3 rotation_axis;
		switch (key) {
		case 'Q': case 'q':
		case 'A': case 'a':
			rotation_axis = glm::vec3(1.0f, 0.0f, 0.0f); // Ox
			break;
		case 'W': case 'w':
		case 'S': case 's':
			rotation_axis = glm::vec3(0.0f, 1.0f, 0.0f); // Oy
			break;
		case 'E': case 'e':
		case 'D': case 'd':
			rotation_axis = glm::vec3(0.0f, 0.0f, 1.0f); // Oz
			break;
		}
		model = glm::rotate(model,
		                    rotation_angle,
		                    rotation_axis);
		break;
	}
}


// obsługa myszy
struct mouse_info {
	int buttons;
	int state;  // 'action' w GLFW
	int xpos, ypos; // typ double w GLFW
} mouse_info;

// callback klawiszy myszy
void mouse(int button, int state, int x, int y)
{
	if (state == BUTTON_DOWN) {
		mouse_info.buttons |= 1<<button;
	} else if (state == BUTTON_UP) {
		mouse_info.buttons = 0;
	}

	eprintf("- mouse button %d in state %d {buttons=0x%02x} "
			"at (%d,%d), was (%d,%d)\n",
			button, state, mouse_info.buttons,
			x, y, mouse_info.xpos, mouse_info.ypos);

	mouse_info.xpos = x;
	mouse_info.ypos = y;
}

// callback ruchu wskaźnika myszy gdy wciśnięty klawisz
void motion(int x, int y)
{
	float dx, dy;

	dx = x - mouse_info.xpos;
	dy = y - mouse_info.ypos;

	if (mouse_info.buttons) {
		eprintf("- motion active, buttons 0x%02x (%s) "
		        "at (%d, %d), delta = (%.2g, %.2g)\n", mouse_info.buttons,
		        (mouse_info.buttons == (1<<MOUSE_LEFT_BUTTON))   ? "left" :
		        (mouse_info.buttons == (1<<MOUSE_RIGHT_BUTTON))  ? "right" :
		        (mouse_info.buttons == (1<<MOUSE_MIDDLE_BUTTON)) ? "middle" :
		        "[other]", x, y, dx, dy);
	}

	if (mouse_info.buttons & (1<<MOUSE_LEFT_BUTTON)) {
		//rotate_x += dy * 0.2;
		//rotate_y += dx * 0.2;
		//model = glm::rotate(model, glm::radians(dy * 0.2f), glm::vec3(1.0f, 0.0f, 0.0f));
		//model = glm::rotate(model, glm::radians(dx * 0.2f), glm::vec3(0.0f, 1.0f, 0.0f));

		arcball.updateDragArc(mouse_info.xpos, mouse_info.ypos, x, y);
#ifndef NDEBUG
		arcballHelper.fprintf_arcball_info(stderr, arcball);
#endif
		model = arcball.adjustViewRotationMatrix(model);
	} else if (mouse_info.buttons & (1<<MOUSE_RIGHT_BUTTON)) {
		//translate_z += dy * 0.01;
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, dy * 0.01f));
	}

	mouse_info.xpos = x;
	mouse_info.ypos = y;
}

void print_help(void)
{
	printf("Use left/right/up/down to move.\n");
	printf("Use pageup/pagedown to change the horizontal scale.\n");
	printf("Press home to reset the position and scale.\n");
	printf("Press F1 to toggle showing view cube.\n");
	printf("Press F2 to toggle perspective.\n");
	printf("Press F3 to toggle rotation.\n");
	printf("Press F4 to toggle polygon offset.\n");
#ifndef NO_CUDA
	printf("Press F5 to toggle calculation on GPU.\n");
#endif /* NO_CUDA */
	printf("Use '<'/'>' to change cube line width.\n");
	printf("Use 'q'/'a' to rotate along Ox axis.\n");
	printf("Use 'w'/'s' to rotate along Oy axis.\n");
	printf("Use 'e'/'d' to rotate along Oz axis.\n");
	printf("Press end to reset rotation.\n");
}

int main(int argc, char *argv[])
{
	eprintf("[%s]\n", __func__);

	init_window(&argc, argv, WINDOW_WIDTH, WINDOW_HEIGHT);
	GLenum glew_status = glewInit();

	if (GLEW_OK != glew_status) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "No support for OpenGL 2.0 found\n");
		fprintf(stderr, "OpenGL version %s (%s:%s)\n",
		        glGetString(GL_VERSION),
		        glGetString(GL_VENDOR), glGetString(GL_RENDERER));
		return 1;
	}

	glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, line_width_range);
	eprintf("- supported aliased line width range: %f .. %f\n",
	        line_width_range[0], line_width_range[1]);
	if (cube_line_width < line_width_range[0])
		cube_line_width = line_width_range[0];
	if (cube_line_width > line_width_range[1])
		cube_line_width = line_width_range[1];

	print_help();

	if (init_resources()) {
		init_draw_callbacks(display);
		init_event_callbacks(special, keyboard, mouse, motion);
		run_event_loop();
	}

	done_window();
	//free_gl_resources();
	free_resources();
	return 0;
}

/*
 * Local Variables:
 * encoding: utf-8-unix
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 */
