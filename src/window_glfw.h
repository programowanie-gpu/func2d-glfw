#pragma once
#ifndef WINDOW_GLFW_H
#define WINDOW_GLFW_H

#if !(defined(USE_GLFW2) || defined(USE_GLFW3))
#define USE_GLFW2
#warning "Using GLFW 2.x by default"
#endif

#if   defined(USE_GLFW2)
#include <GL/glfw.h>
#elif defined(USE_GLFW3)
#include <GLFW/glfw3.h>
#else
#error "Unknown GLFW version"
#endif


/* normal keys */
#ifdef GLFW_KEY_ESCAPE
#define KEY_ESC        GLFW_KEY_ESCAPE
#else
#define KEY_ESC        GLFW_KEY_ESC
#endif

/* special keys */
#define KEY_F1         GLFW_KEY_F1
#define KEY_F2         GLFW_KEY_F2
#define KEY_F3         GLFW_KEY_F3
#define KEY_F4         GLFW_KEY_F4
#define KEY_F5         GLFW_KEY_F5
#define KEY_LEFT       GLFW_KEY_LEFT
#define KEY_RIGHT      GLFW_KEY_RIGHT
#define KEY_UP         GLFW_KEY_UP
#define KEY_DOWN       GLFW_KEY_DOWN
#ifdef GLFW_KEY_PAGEUP
#define KEY_PAGE_UP    GLFW_KEY_PAGEUP
#define KEY_PAGE_DOWN  GLFW_KEY_PAGEDOWN
#else
#define KEY_PAGE_UP    GLFW_KEY_PAGE_UP
#define KEY_PAGE_DOWN  GLFW_KEY_PAGE_DOWN
#endif
#define KEY_HOME       GLFW_KEY_HOME
#define KEY_END        GLFW_KEY_END

// nie zdefiniowane w GLFW 3.x
#ifndef GLFW_KEY_SPECIAL
#define GLFW_KEY_SPECIAL  255
#endif

/* actions, or key/button status */
#define BUTTON_UP      GLFW_RELEASE
#define BUTTON_DOWN    GLFW_PRESS

/* mouse buttons */
#define MOUSE_LEFT_BUTTON    GLFW_MOUSE_BUTTON_LEFT
#define MOUSE_MIDDLE_BUTTON  GLFW_MOUSE_BUTTON_MIDDLE
#define MOUSE_RIGHT_BUTTON   GLFW_MOUSE_BUTTON_RIGHT

#include "window_common.h"

#endif /* WINDOW_GLFW_H */
