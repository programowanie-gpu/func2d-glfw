#include "graph_func.h"

#include <GL/glew.h>
#include <math.h>

void func_xy_from_range(float xmin, float xmax,
                        float *scale, float *offset)
{
	(*scale)  = 2.0/fabs(xmax - xmin);
	(*offset) = -0.5*(xmin + xmax);
}

void range_from_func_xy(float scale, float offset,
                        float *xmin, float *xmax)
{
	(*xmin) = -1.0/scale - offset;
	(*xmax) =  1.0/scale - offset;
}

void scale_offset_from_ranges(float xmin, float xmax,
                              float ymin, float ymax,
                              float *scale, float *offset_x, float *offset_y)
{
	float xscale, yscale;

	func_xy_from_range(xmin, xmax, &xscale, offset_x);
	func_xy_from_range(ymin, ymax, &yscale, offset_y);

	(*scale) = fmin(xscale, yscale);
}

void calc_values(GLfloat *values, int Nx, int Ny, float zscale,
                 float offset_x, float offset_y, float xyscale)
{
	for (int i = 0; i < Ny; i++) {
		for (int j = 0; j < Nx; j++) {
			float x = func_xy_pos(grid_pos(j, Nx), xyscale, offset_x);
			float y = func_xy_pos(grid_pos(i, Ny), xyscale, offset_y);
			float z = func2d(x, y);

			values[i*Nx + j] = z * zscale;
		}
	}
}
