#pragma once

#ifndef ARCBALL_HELPER_HPP
#define ARCBALL_HELPER_HPP

/*
 * Inspiracja:
 *  - https://github.com/mharrys/arcball
 */

#include <stdio.h>

#include "arcball.hpp"

/**
 * The responsibility of this class is to show graphical helpers for a
 * arcball, in other words to visualize arcball-helped rotation
 */
class ArcballHelper {
public:
	// Construct empty arcball helper.
	ArcballHelper() {};
	// Print inforation about arcball state
	void fprintf_arcball_info(FILE *f, Arcball const &arcball);
};

#endif /* ARCBALL_HELPER_HPP */
