#pragma once
#ifndef GRAPH_FUNC_GPU_H
#define GRAPH_FUNC_GPU_H

#include <GL/glew.h>   // potrzebne do definicji typu GLfloat

void calc_values_GPU(GLfloat *values_dev, int Nx, int Ny, float zscale,
                     float offset_x, float offset_y, float xyscale);

#endif /* GRAPH_FUNC_GPU_H */
