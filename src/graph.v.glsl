attribute vec2 coord2d;
attribute float value;
varying vec4 f_color;

uniform mat4 vertex_transform;

// A collection of GLSL fragment shaders to draw color maps.
//   https://github.com/kbinani/glsl-colormap/
// The MIT License.
vec4 colormap(float x);

void main(void) {
	gl_Position = vertex_transform * vec4(coord2d, value, 1);
	f_color = colormap(0.5*value + 0.5); // -1..1 -> 0..1
}

// https://github.com/kbinani/glsl-colormap/blob/master/shaders/MATLAB_jet.frag
float colormap_red(float x) {
	if (x < 0.7) {
		return  4.0 * x - 1.5;
	} else {
		return -4.0 * x + 4.5;
	}
}

float colormap_green(float x) {
	if (x < 0.5) {
		return  4.0 * x - 0.5;
	} else {
		return -4.0 * x + 3.5;
	}
}

float colormap_blue(float x) {
	if (x < 0.3) {
	   return  4.0 * x + 0.5;
	} else {
	   return -4.0 * x + 2.5;
	}
}

vec4 colormap(float x) {
	float r = clamp(colormap_red(x),   0.0, 1.0);
	float g = clamp(colormap_green(x), 0.0, 1.0);
	float b = clamp(colormap_blue(x),  0.0, 1.0);
	return vec4(r, g, b, 1.0);
}
