#pragma once
#ifndef GRAPH_FUNC_H
#define GRAPH_FUNC_H

#include <math.h>    // potrzebne do funkcji matematycznych w func2d
#include <GL/glew.h> // definicja typu GLfloat w interfejsie calc_values

#ifdef NO_CUDA
#define __host__
#define __device__
#endif

__host__ __device__
inline float func2d(float x, float y)
{
	float d = hypotf(x, y - 0.3) * 4.0;
	return (1 - d*d) * expf((d * d + -8.0*x*y - x) / -2.0);
}

// zwraca wartość -1..1
__host__ __device__
inline double grid_pos(int i, int N)
{
	int ioff = (N - 1)/2;
	double scale = 1.0/ioff;

	return scale*(i - ioff);
}

__host__ __device__
inline double func_xy_pos(double pos, double scale, double offset)
{
	return (pos/scale - offset);
}


void func_xy_from_range(float xmin, float xmax,
                        float *scale, float *offset);
void range_from_func_xy(float scale, float offset,
                        float *xmin, float *xmax);
void scale_offset_from_ranges(float xmin, float xmax,
                              float ymin, float ymax,
                              float *scale, float *offset_x, float *offset_y);

void calc_values(GLfloat *values, int Nx, int Ny, float zscale,
                 float offset_x, float offset_y, float  xyscale);


#endif /* GRAPH_FUNC_H */
