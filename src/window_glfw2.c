#include <stdio.h>
#include <stdlib.h>
#include <GL/glfw.h>

#include "window_glfw.h"

static struct callbacks {
	special_callback_t  special;
	keyboard_callback_t keyboard;
	display_callback_t  display;
	mouse_callback_t    mouse;
} callbacks;
static int window_should_close = GL_FALSE;

/* ...................................................................... */

int elapsed_time_ms(void)
{
	double curr_time = glfwGetTime(); // w sekundach
	return (int)(curr_time*1000.0); // 1000 ms = 1 s
}

void finalize_display(void)
{
	glfwSwapBuffers();
}

void redisplay(void)
{
	callbacks.display();
}



void init_window(int *argc, char *argv[], int window_width, int window_height)
{
	/* inicjalizacja biblioteki GLFW */
	if (!glfwInit()) {
		fprintf(stderr, "Error initializing GLFW\n");
		exit(1);
	}

	/* opcjonalnie: wypisz informacje o wersji biblioteki i OpenGL */
	int major, minor, rev;
	glfwGetVersion(&major, &minor, &rev);
	printf("GLFW %d.%d.%d initialized\n", major, minor, rev);

	/* utworzenie okna trybu okienkowego i jego kontekstu OpenGL */
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 0);
	// profile OpenGL istnieją od OpenGL 3.2
	//glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	int res = glfwOpenWindow(window_width, window_height,
	                         8, 8, 8, // RGB mode, 8 bitów na składową koloru
	                         8,       // kanał alpha, 8 bitów
	                         24,      // bufor głębokości, 24 bity
	                         0,       // brak bufora stencil
	                         GLFW_WINDOW);
	if (!res) {
		fprintf(stderr, "Error creating window\n");
		glfwTerminate();
		exit(1);
	}
	glfwSetWindowTitle("My Graph");

	/* ustawienie parametrów */
	//glfwEnable(GLFW_MOUSE_CURSOR); // kursor widoczny, pozycja ograniczona do okna
	//glfwEnable(GLFW_STICKY_KEYS);  // reakcja na klawisze które zostały przyciśnięte i zwolnione
}

void done_window(void)
{
	glfwTerminate();
}

static void GLFWCALL GLFWKeyCallback(int key, int action)
{
	if (action != GLFW_PRESS)
		return;

	int x, y;
	glfwGetMousePos(&x, &y);

	if (key > GLFW_KEY_SPECIAL && callbacks.special) {
		callbacks.special(key, x, y);
	} else if (callbacks.keyboard) {
		callbacks.keyboard(key, x, y);
	}
}

static void GLFWMouseButtonCallback(int button, int action)
{
	if (!callbacks.mouse)
		return;

	int x, y;
	glfwGetMousePos(hwindow, &x, &y);

	callbacks.mouse(button, action, x, y);
}


static int GLFWCALL GLFWCloseCallback(void)
{
	window_should_close = GL_TRUE;
	return GL_TRUE;
}

void init_event_callbacks(special_callback_t  special,
                          keyboard_callback_t keyboard,
                          mouse_callback_t    mouse,
                          motion_callback_t   motion)
{
	callbacks.special  = special;
	callbacks.keyboard = keyboard;
	callbacks.mouse    = mouse;
	glfwSetKeyCallback(GLFWKeyCallback);
	// UWAGA: nie obsługuje scrollowania za pomocą kółka myszy
	glfwSetMouseButtonCallback(GLFWMouseButtonCallback);
	// UWAGA: glutMotionFunc, glutPassiveMotionFunc w jednym
	glfwSetCursorPosCallback(motion);
}

void init_draw_callbacks(display_callback_t display)
{
	callbacks.display = display;
	glfwSetWindowRefreshCallback(display);

	/* TODO: wybrać odpowiednie miejsce */
	glfwSetWindowCloseCallback(GLFWCloseCallback);
}

void run_event_loop(void)
{
	while (!window_should_close) {
		/* renderowanie */
		callbacks.display();

		/* display() zawiera na końcu finalize_display() */
		//glfwSwapBuffers();

		/* zbieranie zdarzeń i reakcja na nie */
		glfwPollEvents();
	}
}

void exit_event_loop(void)
{
	window_should_close = GL_TRUE;
}
