varying vec4 graph_coord;
varying vec4 f_color;

uniform vec4 color;

void main(void) {
	float factor;

	if (gl_FrontFacing)
		factor = 1.0;
	else
		factor = 0.5;

	gl_FragColor = f_color * color * factor;
}
