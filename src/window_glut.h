#pragma once
#ifndef WINDOW_GLUT_H
#define WINDOW_GLUT_H

#ifndef USE_FREEGLUT
#include <GL/glut.h>
#else  /* USE_FREEGLUT */
#include <GL/freeglut.h>
#endif /* USE_FREEGLUT */


/* normal keys */
#define KEY_ESC        27

/* special keys */
#define KEY_F1         GLUT_KEY_F1
#define KEY_F2         GLUT_KEY_F2
#define KEY_F3         GLUT_KEY_F3
#define KEY_F4         GLUT_KEY_F4
#define KEY_F5         GLUT_KEY_F5
#define KEY_LEFT       GLUT_KEY_LEFT
#define KEY_RIGHT      GLUT_KEY_RIGHT
#define KEY_UP         GLUT_KEY_UP
#define KEY_DOWN       GLUT_KEY_DOWN
#define KEY_PAGE_UP    GLUT_KEY_PAGE_UP
#define KEY_PAGE_DOWN  GLUT_KEY_PAGE_DOWN
#define KEY_HOME       GLUT_KEY_HOME
#define KEY_END        GLUT_KEY_END

/* actions, or key/button status */
#define BUTTON_UP      GLUT_UP
#define BUTTON_DOWN    GLUT_DOWN

/* mouse buttons */
#define MOUSE_LEFT_BUTTON    GLUT_LEFT_BUTTON
#define MOUSE_MIDDLE_BUTTON  GLUT_MIDDLE_BUTTON
#define MOUSE_RIGHT_BUTTON   GLUT_RIGHT_BUTTON

#include "window_common.h"

#endif /* WINDOW_GLUT_H */
