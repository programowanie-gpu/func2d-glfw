#pragma once

#ifndef WINDOW_COMMON_H
#define WINDOW_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*special_callback_t)(int key, int x, int y);
typedef void (*keyboard_callback_t)(unsigned char key, int x, int y);
typedef void (*display_callback_t)(void);
typedef void (*mouse_callback_t)(int button, int state, int x, int y);
typedef void (*motion_callback_t)(int x, int y);

int elapsed_time_ms(void);

void finalize_display(void);
void redisplay(void);

void init_window(int *argc, char *argv[], int window_width, int window_height);
void init_event_callbacks(special_callback_t special,
                          keyboard_callback_t keyboard,
                          mouse_callback_t mouse,
                          motion_callback_t motion);
void init_draw_callbacks(display_callback_t display);
void done_window(void);

void run_event_loop(void);
void exit_event_loop(void);

#ifdef __cplusplus
}
#endif

#endif /* WINDOW_COMMON_H */
