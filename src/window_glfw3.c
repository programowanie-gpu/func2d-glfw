#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <GLFW/glfw3.h>

#ifndef USE_GLFW3
#define USE_GLFW3
#endif
#include "window_glfw.h"

static struct callbacks {
	special_callback_t  special;
	keyboard_callback_t keyboard;
	display_callback_t  display;
	mouse_callback_t    mouse;
	motion_callback_t   motion;
} callbacks;
static GLFWwindow* hwindow = NULL;

/* ...................................................................... */

int elapsed_time_ms(void)
{
	double curr_time = glfwGetTime(); // w sekundach
	return (int)(curr_time*1000.0); // 1000 ms = 1 s
}

void finalize_display(void)
{
	glfwSwapBuffers(hwindow);
}

void redisplay(void)
{
	callbacks.display();
}

// odpowiedzialne za informacje o błędach takich jak:
// - X11: Failed to open X display
//   (freeglut: failed to open display 'localhost:10.0')
void GLFWErrorCallback(int error_code, const char* description)
{
	fprintf(stderr, "GLFW error %d - %s\n", error_code, description);
	exit(EXIT_FAILURE);
}


void init_window(int *argc, char *argv[], int window_width, int window_height)
{
	/* informuj o błędach GLFW */
	glfwSetErrorCallback(GLFWErrorCallback);

	/* inicjalizacja biblioteki GLFW */
	if (!glfwInit()) {
		fprintf(stderr, "Error initializing GLFW\n");
		exit(EXIT_FAILURE);
	}

	/* opcjonalnie: wypisz informacje o wersji biblioteki i OpenGL */
	int major, minor, rev;
	glfwGetVersion(&major, &minor, &rev);
	printf("GLFW %d.%d.%d initialized\n", major, minor, rev);

	/* utworzenie okna trybu okienkowego i jego kontekstu OpenGL */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	// profile OpenGL istnieją od OpenGL 3.2
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	/* odpowiednik GLUT_DOUBLE, GLUT_RGB, GLIT_DEPTH jest domyślny */
	hwindow = glfwCreateWindow(window_width, window_height,
	                           "My Graph", // tytuł okna
	                           NULL, NULL);
	if (!hwindow) {
		fprintf(stderr, "Error creating window\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	/* uczyń kontekst okna bieżącym */
	glfwMakeContextCurrent(hwindow);
}

void done_window(void)
{
	glfwDestroyWindow(hwindow);
	glfwTerminate();
}

static void GLFWKeyCallback(GLFWwindow *hwindow, int key,
                            int scancode, int action, int mode)
{
	if (action != GLFW_PRESS && action != GLFW_REPEAT)
		return;

	double x, y;
	glfwGetCursorPos(hwindow, &x, &y);

	if (key > GLFW_KEY_SPECIAL && callbacks.special) {
		callbacks.special(key, x, y);
	} else if (callbacks.keyboard) {
		if (!(mode & GLFW_MOD_SHIFT) && isupper(key)) {
			key = tolower(key);
		}
		callbacks.keyboard(key, x, y);
	}
}

static void GLFWMouseButtonCallback(GLFWwindow* window, int button,
                                    int action, int mods)
{
	if (!callbacks.mouse)
		return;

	double x, y;
	glfwGetCursorPos(hwindow, &x, &y);

	callbacks.mouse(button, action, x, y);
}

static void GLFWCursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (!callbacks.mouse || !callbacks.motion)
		return;

	// powinnyśmy sprawdzić, czy przycisk został wciśnięty
	callbacks.motion(xpos, ypos);
}

void init_event_callbacks(special_callback_t  special,
                          keyboard_callback_t keyboard,
                          mouse_callback_t    mouse,
                          motion_callback_t   motion)
{
	callbacks.special  = special;
	callbacks.keyboard = keyboard;
	callbacks.mouse    = mouse;
 	callbacks.motion   = motion;
	glfwSetKeyCallback(hwindow, GLFWKeyCallback);
	// UWAGA: nie obsługuje scrollowania za pomocą kółka myszy
	glfwSetMouseButtonCallback(hwindow, GLFWMouseButtonCallback);
	// UWAGA: glutMotionFunc, glutPassiveMotionFunc w jednym
	glfwSetCursorPosCallback(hwindow, GLFWCursorPositionCallback);
}

void GLFWRefreshCallback(GLFWwindow *window)
{
	callbacks.display();
}

void init_draw_callbacks(display_callback_t display)
{
	callbacks.display = display;
	glfwSetWindowRefreshCallback(hwindow, GLFWRefreshCallback);
}

void run_event_loop(void)
{
	while (!glfwWindowShouldClose(hwindow)) {
		/* renderowanie */
		callbacks.display();

		/* display() zawiera na końcu finalize_display() */
		//glfwSwapBuffers(hwindow);

		/* zbieranie zdarzeń i reakcja na nie */
		glfwPollEvents();
	}
}

void exit_event_loop(void)
{
	glfwSetWindowShouldClose(hwindow, GL_TRUE);
}
