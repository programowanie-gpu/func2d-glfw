attribute vec4 vertex;

uniform mat4 vertex_transform;

void main(void)
{
	gl_Position = vertex_transform * vertex;
}
