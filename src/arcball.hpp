#pragma once

#ifndef ARCBALL_HPP
#define ARCBALL_HPP

/*
 * Źródła:
 *  - https://github.com/subokita/Arcball
 *  - https://github.com/mharrys/arcball
 */

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

struct Arc {
	glm::vec3 from;
	glm::vec3 to;
};

class ArcballHelper;

class Arcball {
	friend ArcballHelper;
private:
	int windowWidth;
	int windowHeight;
	int mouseEvent;
	float rollSpeed;
	float angle;
	glm::quat orientation;
	glm::vec3 prevPos;
	glm::vec3 currPos;
	glm::vec3 camAxis;

	bool xAxis;
	bool yAxis;

	bool rotated;

public:
	Arcball(int window_width, int window_height,
	        float roll_speed = 1.0f,
	        bool x_axis = true, bool y_axis = true);
	glm::vec3 toScreenCoord(double x, double y);
	glm::vec3 toBallCoord(double x, double y);

	void updateDragArc(double xprev, double yprev,
	                   double xcurr, double ycurr);

	glm::mat4 createViewRotationMatrix();
	glm::mat4 adjustViewRotationMatrix(glm::mat4 &matrix);
	glm::mat4 createModelRotationMatrix(glm::mat4 &view_matrix);
};

#endif /* ARCBALL_HPP */
