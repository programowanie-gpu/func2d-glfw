#ifndef USE_FREEGLUT
#include <GL/glut.h>
#else  /* USE_FREEGLUT */
#include <GL/freeglut.h>
#endif /* USE_FREEGLUT */

#include "window_glut.h"


int elapsed_time_ms(void)
{
	return glutGet(GLUT_ELAPSED_TIME);
}

void finalize_display(void)
{
	glutSwapBuffers();
}

void redisplay(void)
{
	glutPostRedisplay();
}

void init_window(int *argc, char *argv[], int window_width, int window_height)
{
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
#ifdef USE_FREEGLUT
	glutInitContextVersion(2, 0); // minimalna wersja OpenGL 2.0
	// profile OpenGL istnieją od OpenGL 3.2
	//glutInitContextProfile(GLUT_CORE_PROFILE);
	// to do your own cleanup
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
#endif /* USE_FREEGLUT */
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("My Graph");
}

void init_event_callbacks(special_callback_t  special,
                          keyboard_callback_t keyboard,
                          mouse_callback_t    mouse,
                          motion_callback_t   motion)
{
	glutSpecialFunc(special);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
}

void init_draw_callbacks(display_callback_t display)
{
	glutDisplayFunc(display);
	glutIdleFunc(display);
}

void done_window(void)
{
#ifdef USE_FREEGLUT_XXXXXXXX
	/*
	 * Błąd w FreeGLUT - użycie tej funkcji daje błąd:
	 *   freeglut  ERROR:  Function <glutLeaveMainLoop> called without first calling 'glutInit'.
	 * mimo że glutInit jest poprawnie zainicjalizowany
	 */
	glutLeaveMainLoop();
#else
	//glutDestroyWindow(glutGetWindow());
	exit(0);
#endif /* USE_FREEGLUT */
}

void run_event_loop(void)
{
	glutMainLoop();
}

void exit_event_loop(void)
{
#ifdef USE_FREEGLUT
	/*
	 * Błąd w FreeGLUT - użycie tej funkcji daje błąd:
	 *   freeglut  ERROR:  Function <glutLeaveMainLoop> called without first calling 'glutInit'.
	 * mimo że glutInit jest poprawnie zainicjalizowany
	 */
	exit(0);
	glutLeaveMainLoop();
#else
	exit(0);
#endif /* USE_FREEGLUT */
}
