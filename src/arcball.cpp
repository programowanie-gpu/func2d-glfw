#include "arcball.hpp"

/*
 * Źródła:
 *  - https://github.com/subokita/Arcball
 *  - https://github.com/mharrys/arcball
 */

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_inverse.hpp>

/**
 * Constructor.
 * @param roll_speed the speed of rotation
 */
Arcball::Arcball(int window_width, int window_height, float roll_speed, bool x_axis, bool y_axis)
{
	windowWidth  = window_width;
	windowHeight = window_height;

	mouseEvent = 0;
	rollSpeed  = roll_speed;
	angle      = 0.0f;
	camAxis    = glm::vec3(0.0f, 1.0f, 0.0f);

	orientation = glm::quat(angle, camAxis);

	xAxis = x_axis;
	yAxis = y_axis;

	rotated = false;
}

/**
 * Convert the mouse cursor coordinate on the window (i.e. from (0,0) to (windowWidth, windowHeight))
 * into normalized screen coordinate (i.e. (-1, -1) to (1, 1)
 */
glm::vec3 Arcball::toScreenCoord(double x, double y)
{
	glm::vec3 coord(0.0f);

	if (xAxis)
		coord.x =  (2 * x - windowWidth ) / windowWidth;

	if (yAxis)
		coord.y = -(2 * y - windowHeight) / windowHeight;

	/* Clamp it to border of the windows, comment these codes to allow rotation when cursor is not over window */
	coord.x = glm::clamp(coord.x, -1.0f, 1.0f);
	coord.y = glm::clamp(coord.y, -1.0f, 1.0f);
	coord.z = 0.0f;

	return coord;
}

/**
 * Convert the mouse cursor coordinate (mouse position) in pixels
 * into coordinates on ball centered at (0,0,0)
 */
glm::vec3 Arcball::toBallCoord(double x, double y)
{
	glm::vec3 coord = toScreenCoord(x, y);

	float length_squared = glm::length2(coord);
	if (length_squared <= 1.0)
		// point on ball
		coord.z = sqrt(1.0 - length_squared);
	else
		// set to nearest point on ball
		// coord *= (1.0 / sqrt(length_squared));
		coord = glm::normalize(coord);

	return coord;
}

void Arcball::updateDragArc(double xprev, double yprev,
                            double xcurr, double ycurr)
{
	rotated = false;

	/* sanity check 1 */
	if (xprev == xcurr && yprev == ycurr)
		return;

	/* start of drag and current position, points on ball */
	prevPos = toBallCoord(xprev, yprev);
	currPos = toBallCoord(xcurr, ycurr);

	/* sanity check 2 */
	if (glm::distance2(prevPos, currPos) < 1e-10)
		return;

	/* Calculate the angle in radians, and clamp it between 0 and 90 degrees */
	float w = glm::dot(prevPos, currPos);
	angle   = acos(glm::min(1.0f, w));

	/* sanity check 3 */
	if (fabs(w) < 1e-10)
		return;

	/* Cross product to get the rotation axis, but it's still in camera coordinate */
	camAxis = glm::cross(prevPos, currPos);
	if (!yAxis) // coord.y == 0
		camAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	if (!xAxis) // coord.x == 0
		camAxis = glm::vec3(1.0f, 0.0f, 0.0f);

	/* sanity check 4 */
	if (camAxis == glm::vec3(0.0f, 0.0f, 0.0f))
		return;

	/* Quaternion that represents the rotation from initial point to end point */
	glm::quat drag(w, camAxis);

	/* Product of two quaternions give the combination of rotations */
	orientation = glm::normalize(drag * orientation);

	/* There was rotation */
	rotated = true;
}

/**
 * Create rotation matrix within the camera coordinate,
 * multiply this matrix with view matrix to rotate the camera
 */
glm::mat4 Arcball::createViewRotationMatrix()
{
	// angle in radians (acos), GLM also uses radians thanks to GLM_FORCE_RADIANS
	if (rotated)
		return glm::rotate(angle * rollSpeed, camAxis);
	else
		return glm::mat4(1.0f);
}

glm::mat4 Arcball::adjustViewRotationMatrix(glm::mat4 &matrix)
{
	if (rotated)
		return glm::rotate(matrix, angle * rollSpeed, camAxis);
	else
		return matrix;
}

/**
 * Create rotation matrix within the world coordinate,
 * multiply this matrix with model matrix to rotate the object
 */
glm::mat4 Arcball::createModelRotationMatrix(glm::mat4 &view_matrix)
{
	if (!rotated)
		return glm::mat4(1.0f);

	glm::vec3 axis = glm::inverse(glm::mat3(view_matrix)) * camAxis;
	return glm::rotate(glm::radians(angle) * rollSpeed, axis);
}
