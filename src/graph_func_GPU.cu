#include "graph_func.h"
#include "graph_func_GPU.h"

__global__ static void
calc_values_kernel(GLfloat *values_dev, int Nx, int Ny, float zscale,
                   float offset_x, float offset_y, float xyscale)
{
	int i = blockIdx.y*blockDim.y + threadIdx.y;
	int j = blockIdx.x*blockDim.x + threadIdx.x;

	while (i < Ny) {
		while (j < Nx) {
			float x = func_xy_pos(grid_pos(j, Nx), xyscale, offset_x);
			float y = func_xy_pos(grid_pos(i, Ny), xyscale, offset_y);
			float z = func2d(x, y);

			values_dev[i*Nx + j] = z * zscale;

			j += blockDim.x*gridDim.x;
		}
		i += blockDim.y*gridDim.y;
	}
}

void calc_values_GPU(GLfloat *values_dev, int Nx, int Ny, float zscale,
                     float offset_x, float offset_y, float xyscale)
{
	dim3 threadsPerBlock = dim3(16, 16);
	dim3 blocksPerGrid = dim3((Nx + threadsPerBlock.x - 1)/threadsPerBlock.x,
	                          (Ny + threadsPerBlock.y - 1)/threadsPerBlock.y);

	calc_values_kernel<<<blocksPerGrid, threadsPerBlock>>>(values_dev, Nx, Ny,
	                                                       zscale, offset_x, offset_y, xyscale);
}
