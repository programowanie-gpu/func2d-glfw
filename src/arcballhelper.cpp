#include "arcballhelper.hpp"

void ArcballHelper::fprintf_arcball_info(FILE *f, Arcball const &arcball)
{
	fprintf(f, "= arcball: angle = %g, axis = (%g, %g, %g)\n",
	        arcball.angle, arcball.camAxis.x, arcball.camAxis.y, arcball.camAxis.z);
	fprintf(f, "= arcball: prev = (% .6f, % .6f, % .6f)\n"
			   "= arcball: curr = (% .6f, % .6f, % .6f)\n",
			arcball.prevPos.x, arcball.prevPos.y, arcball.prevPos.z,
			arcball.currPos.x, arcball.currPos.y, arcball.currPos.z);
}

/* end of 'arcballhelper.cpp' */
