func2d-glfw - Demo wykresu funkcji 2d w OpenGL
==============================================

Ten program ma (w zamierzeniach) demonstrować łączenie obliczeń na GPU
za pomocą CUDA C oraz tworzenia wizualizacji obliczeń za pomocą OpenGL.
Demonstracja odbywa się przez wygenerowanie i wyświetlenia wykresu 
(w zamierzeniach animowanej) funkcji 2d, $z = func(x,y)$.

Tworzenie grafiki odbywa się z wykorzystaniem nowoczesnego OpenGL (shadery).
Ponieważ większość tutoriali nowoczesnego OpenGL używa biblioteki GLFW, więc
wsparcie do tej biblioteki było zaplanowane od początku.  Obecnie program
obsługuje następujące biblioteki okienkowe:

  * GLUT (OpenGL Utility Toolkit), a dokładniej FreeGLUT, *domyślne*,
    kompilując w `src/` za pomocą `make`
  * GLFW 2.x, kompilując w `src/` za pomocą `make USE_GLUT=0`
  * GLFW 3.x, kompilując w `src/` za pomocą `make USE_GLFW3=1`

Program używa biblioteki GLEW do zarządzania rozszerzeniami OpenGL.

Repozytorium projektu dostępne jest pod
<https://gitlab.com/programowanie-gpu/func2d-glfw>

Historia powstania
------------------

Program powstał w semestrze letnim roku akademickiego 2015/2016 na
przedmiocie do wyboru ["Programowanie GPU" (1000-I2PGPU)][1], który odbywa
się na Wydziale Matematyki i Informatyki Uniwersytetu Mikołaja Kopernika
w Toruniu.

[1]: https://usosweb.umk.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1000-I2PGPU

Podstawą pierwszej wersji projektu był kod programu z wikiksiążki
["OpenGL Programming"][2] na [WikiBooks](https://en.wikibooks.org/),
a dokładniej sekcji "Graph 05" z podrozdziału ["The scientific arc"][3].
Pełny kod całej książki, w tym tego podrozdziału, dostępny jest pod adresem
<https://gitlab.com/wikibooks-opengl/modern-tutorials>

[2]: https://en.wikibooks.org/wiki/OpenGL_Programming
[3]: https://en.wikibooks.org/wiki/OpenGL_Programming#The_scientific_arc
