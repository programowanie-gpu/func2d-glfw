#include <math.h>

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

// zwraca wartość -1..1
double grid_pos(int i, int N)
{
	int ioff = (N - 1)/2;
	double scale = 1.0/ioff;

	return scale*(i - ioff);
}

int pos_to_grid(double pos, int N)
{
	return ((N-1)*(pos + 1.0))/2;
}


#define NN 101

TEST_CASE( "grid_pos corner cases", "[pos]" ) {
  REQUIRE( grid_pos(0,    NN) == -1.0 );
  REQUIRE( grid_pos(NN/2, NN) ==  0.0 );
  REQUIRE( grid_pos(NN-1, NN) ==  1.0 );
}

TEST_CASE( "pos_to_grid corner cases", "[pos]") {
  REQUIRE( pos_to_grid(-1.0, NN) == 0 );
  REQUIRE( pos_to_grid( 0.0, NN) == (NN-1)/2 );
  REQUIRE( pos_to_grid( 1.0, NN) == NN-1 );
}

TEST_CASE( "grid_pos <-> pos_to_grid round trip", "[pos]") {
  REQUIRE( pos_to_grid(grid_pos(13,    NN), NN) == 13 );
  REQUIRE( pos_to_grid(grid_pos(NN-11, NN), NN) == NN-11 );
}

double func_xy_pos(double pos, double scale, double offset)
{
	return (pos/scale - offset);
}

TEST_CASE( "func_xy_pos corner cases", "[func_xy]" ) {
  // zoom
  REQUIRE( func_xy_pos(-1.0, 2.0, 0.0) == -0.5 );
  REQUIRE( func_xy_pos( 1.0, 2.0, 0.0) ==  0.5 );
  // offset
  REQUIRE( func_xy_pos(-1.0, 1.0, 1.0) == -2.0 );
  REQUIRE( func_xy_pos( 1.0, 1.0, 1.0) ==  0.0 );
  // zoom + offset
  REQUIRE( func_xy_pos(-1.0, 2.0, 0.3) == -0.5 - 0.3 );
  REQUIRE( func_xy_pos( 1.0, 2.0, 0.3) ==  0.5 - 0.3 );
}

double dx_from_range(double xmin, double xmax, int N)
{
	return fabs(xmax - xmin)/(N-1);
}

double grid_func(int i, double xmin, double dx)
{
	return xmin + i*dx;
}

void func_xy_from_range(double xmin, double xmax,
                        double *scale, double *offset)
{
	(*scale)  = 2.0/fabs(xmax - xmin);
	(*offset) = -0.5*(xmin + xmax);
}

TEST_CASE( "grid_func corner cases", "[grid_func]" ) {
  double xmin = -0.75, xmax = 2.33;
  double dx = dx_from_range(xmin, xmax, NN);

  REQUIRE( grid_func(0,    xmin, dx) == xmin );
  REQUIRE( grid_func(NN-1, xmin, dx) == xmax );
}

TEST_CASE( "func_xy_from_range etc.", "[grid_func]" ) {
  double xmin = -0.75, xmax = 2.33;
  double xscale, xoffset;
  func_xy_from_range(xmin, xmax, &xscale, &xoffset);

  REQUIRE( func_xy_pos(-1.0, xscale, xoffset) == xmin );
  REQUIRE( func_xy_pos( 0.0, xscale, xoffset) == 0.5*(xmin + xmax) );
  REQUIRE( func_xy_pos( 1.0, xscale, xoffset) == xmax );
}
